#!/bin/sh

keyboardid=$(xinput --list | awk -v search="AT Translated Set 2 keyboard" \
    '$0 ~ search {match($0, /id=[0-9]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
)

keyboardmaster=$(xinput --list | awk -v search="Virtual core keyboard" \
    '$0 ~ search {match($0, /id=[0-9]+/);\
                  if (RSTART) \
                    print substr($0, RSTART+3, RLENGTH-3)\
                 }'\
)

isDisabled=$(xinput list | grep "AT Translated Set 2 keyboard.*id=$keyboardid.*\[floating slave\]")

if [ "$isDisabled" != "" ];
then
    echo "Native keyboard attached"
    xinput reattach $keyboardid $keyboardmaster
else
    echo "Native keyboard detached"
    xinput float $keyboardid
fi
